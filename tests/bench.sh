
# bench for running same suite in different shells
# ./bench.sh

[ ! -z "$LEECHT" ] && set "$LEECHT"

SUITE=$(dirname $0)/suite.sh
HERE=$(dirname $0)
export PATH="$HERE/../sbin:$HERE/../sbin/recipes:$PATH"

sh $SUITE sh
ash $SUITE ash
ksh $SUITE ksh
dash $SUITE dash
bash $SUITE bash
