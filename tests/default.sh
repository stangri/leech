#!/bin/sh

[ ! -z "$LEECHT" ] && set "$LEECHT"

. $(dirname $0)/assert.sh

HERE=$(cd "$(dirname "$0")" && pwd)  # current dir
TOOL="$HERE/../sbin/leech-default"
CONFIG="$HERE/../sbin/leech-config"
DL="$HERE/dl"

# tell recipe to download this file
#
URL="file://$HERE/$(basename "$0")"

# clean dl
#
rm -f "$DL"/*

OUTPUT=$(LEECH_DOWNLOADS_DIR="$DL" \
LEECH_URL="$URL" \
LEECH_URL_MD5="default_md5" \
LEECH_TIMEOUT="1" \
$TOOL)

# recipe should
#  1) exit normally
#  2) print output filename to stdout
#  3) actually download file pointed by $URL
#
assert "$? -eq 0"
assert "! -z "$OUTPUT""
assert "-f "$OUTPUT""

# cleanup
#
rm -f "$OUTPUT"
