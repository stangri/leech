#!/bin/sh

[ ! -z "$LEECHT" ] && set "$LEECHT"

. $(dirname $0)/assert.sh

HERE=$(cd "$(dirname "$0")" && pwd)  # current dir
TOOL="$HERE/../sbin/leech"
CONFIG="$HERE/../sbin/leech-config"

SOURCE="$HERE/files/rss.xml"
PROCESSED="$HERE/files/processed.xml"
LUNCH="file://$PROCESSED"  # file/processed.xml

export CONFIG_DIR="$HERE/conf-target-dir"
export DOWNLOADS_DIR="$HERE/dl"

. "$CONFIG"

rm -f "$DOWNLOADS_DIR"/*
rm -f "$DOWNLOADS_DIR/.leech.db"
rm -f "$PROCESSED"
rm -f "$FOODS"

echo -n $LUNCH >"$FOODS"  # put processed XML URL into foods, -n to check proces
cat "$SOURCE" \
	| sed -e "s|NOW|$(date -R)|" \
        | sed -e "s|file://.|file://$HERE|" \
	>"$PROCESSED"  # replace file://./files with absolute paths for cURL

# test that no target dir was recorded w/o $TARGET_DIR set
#
($TOOL >/dev/null)

assert "-z $(cat "$DOWNLOADS_DIR/forgot")"

# for each download a special recipe should be called
# to check that TARGET_DIR is set in recipe
#
export TARGET_DIR="$DOWNLOADS_DIR"

rm -f "$DOWNLOADS_DIR"/*
rm -f "$DOWNLOADS_DIR/.leech.db"

($TOOL >/dev/null)

# test that $TARGET_DIR passed to leech was correctly re-passed to recipe
#
assert "$(cat "$DOWNLOADS_DIR/forgot" | grep "$TARGET_DIR" | wc -l) -eq 1"

# cleanup
#
rm -f "$DOWNLOADS_DIR"/*
rm -f "$DOWNLOADS_DIR"/.leech.db
rm -f "$PROCESSED"
rm -f "$FOODS"
