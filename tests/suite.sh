#!/bin/sh

# suite for running different tests
# suite.sh <SHELL>

[ ! -z "$LEECHT" ] && set "$LEECHT"

TESTS="$(dirname $0)/rfc822tounix.sh \
    $(dirname $0)/leech.sh \
    $(dirname $0)/wild-magic.sh \
    $(dirname $0)/db-cleanup.sh \
    $(dirname $0)/reverse-magic.sh \
    $(dirname $0)/match-test.sh \
    $(dirname $0)/cookies.sh \
    $(dirname $0)/recipe.sh \
    $(dirname $0)/default.sh \
    $(dirname $0)/target-dir.sh"

run_test()
{
    echo -n "$SHELL:$1: "  # print shell and test name

    ($1)  # note that this modification of assert will print dot after each successful check
    RET=$?

    case $RET in
        0)
            echo "OK"
            ;;

        *)
            ;;
    esac
}

SHELL=$1

for CASE in $TESTS; do
    run_test "$CASE"
done
