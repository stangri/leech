# version
VERSION=1.5
BUILD=3
VERSION_STRING="${VERSION}-${BUILD}"

# stuff
BUILD_DIR=pkg_build

# packages
OPTW_TARGET=leech_${VERSION}-${BUILD}_optware_all.ipk
ENTW_TARGET=leech_${VERSION}-${BUILD}_entware_all.ipk
IPK_TARGET=leech_${VERSION}-${BUILD}_all.ipk
DEB_TARGET=leech_${VERSION}-${BUILD}_all.deb

# order matters, optw and entw targets need to be renamed 
# before building ipk target
default: executable optw entw deb ipk

executable:
	chmod +x sbin/*

clean:
	rm -fr "${BUILD_DIR}"
	rm -f "${OPTW_TARGET}"
	rm -f "${ENTW_TARGET}"
	rm -f "${IPK_TARGET}"
	rm -f "${DEB_TARGET}"

deb:
	mkdir -p "${BUILD_DIR}"
	
	# control
	mkdir -p "${BUILD_DIR}/DEBIAN"
	cp build/deb/conffiles "${BUILD_DIR}/DEBIAN/"
	cat build/deb/control | sed -r -e "s|LEECH_VERSION|${VERSION_STRING}|" >"${BUILD_DIR}/DEBIAN/control"
	
	# data
	mkdir -p "${BUILD_DIR}/etc/leech" "${BUILD_DIR}/usr/sbin" "${BUILD_DIR}/usr/share/leech"
	
	cp config/* "${BUILD_DIR}/etc/leech/"
	cp sbin/* "${BUILD_DIR}/usr/sbin/"
	cp share/leech/* "${BUILD_DIR}/usr/share/leech/"
	
	# deb
	fakeroot dpkg -b "${BUILD_DIR}" "${DEB_TARGET}"
	
	# cleanup
	rm -fr "${BUILD_DIR}"

ipk:
	mkdir -p "${BUILD_DIR}"
	
	# control.tar.gz
	mkdir -p "${BUILD_DIR}/CONTROL"
	cp build/ipkg/conffiles "${BUILD_DIR}/CONTROL/"
	cat build/ipkg/control | sed -r -e "s|LEECH_VERSION|${VERSION_STRING}|" >"${BUILD_DIR}/CONTROL/control"
	
	# data.tar.gz
	mkdir -p "${BUILD_DIR}/usr/sbin" "${BUILD_DIR}/usr/share/leech" "${BUILD_DIR}/etc/leech"
	cp config/* "${BUILD_DIR}/etc/leech/"
	cp sbin/* "${BUILD_DIR}/usr/sbin/"
	cp share/leech/* "${BUILD_DIR}/usr/share/leech/"
	
	# leech.ipk
	fakeroot build/ipkg-build "${BUILD_DIR}"
	
	# cleanup
	rm -fr "${BUILD_DIR}"

optw:
	mkdir -p "${BUILD_DIR}"
	
	# control.tar.gz
	mkdir -p "${BUILD_DIR}/CONTROL"
	cp build/optw/conffiles "${BUILD_DIR}/CONTROL/"
	cat build/optw/control | sed -r -e "s|LEECH_VERSION|${VERSION_STRING}|" >"${BUILD_DIR}/CONTROL/control"
	
	# data.tar.gz
	mkdir -p "${BUILD_DIR}/opt/sbin" "${BUILD_DIR}/opt/share/leech" "${BUILD_DIR}/opt/etc/leech"
	cp config/* "${BUILD_DIR}/opt/etc/leech/"
	cp sbin/* "${BUILD_DIR}/opt/sbin/"
	cp share/leech/* "${BUILD_DIR}/opt/share/leech/"
	
	# leech.ipk
	fakeroot build/ipkg-build "${BUILD_DIR}"
	mv "${IPK_TARGET}" "${OPTW_TARGET}"
	
	# cleanup
	rm -fr "${BUILD_DIR}"

entw:
	mkdir -p "${BUILD_DIR}"
	
	# control.tar.gz
	mkdir -p "${BUILD_DIR}/CONTROL"
	cp build/entw/conffiles "${BUILD_DIR}/CONTROL/"
	cat build/entw/control | sed -r -e "s|LEECH_VERSION|${VERSION_STRING}|" >"${BUILD_DIR}/CONTROL/control"
	
	# data.tar.gz
	mkdir -p "${BUILD_DIR}/opt/usr/sbin" "${BUILD_DIR}/opt/usr/share/leech" "${BUILD_DIR}/opt/etc/leech"
	cp config/* "${BUILD_DIR}/opt/etc/leech/"
	cp sbin/* "${BUILD_DIR}/opt/usr/sbin/"
	cp share/leech/* "${BUILD_DIR}/opt/usr/share/leech/"
	
	# leech.ipk
	fakeroot build/ipkg-build "${BUILD_DIR}"
	mv "${IPK_TARGET}" "${ENTW_TARGET}"
	
	# cleanup
	rm -fr "${BUILD_DIR}"
